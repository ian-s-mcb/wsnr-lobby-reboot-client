// WebSocket object
const host = process.env.NODE_ENV === 'development'
  ? 'ws://localhost:5000'
  : 'wss://wsnr-lobby-server.herokuapp.com'
const ws = new WebSocket(host)

export { ws }
