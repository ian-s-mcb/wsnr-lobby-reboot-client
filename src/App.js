import { Modal } from 'bootstrap'
import { useState } from 'react'

import './App.css'
import { ACCEPT_UPDATE_MEM_LIST } from './constants'
import DisconnectModal from './components/DisconnectModal'
import Footer from './components/Footer'
import Lobby from './components/lobby/Lobby'
import NameForm from './components/NameForm'
import { ws } from './wsUtils'

function App () {
  // Clear member list
  const [members, setMembers] = useState([])
  const [name, setName] = useState('')
  const [status, setStatus] = useState('available')
  ws.onclose = e => {
    setMembers([])
    const el = document.getElementById('disconnectModal')
    const modal = new Modal(el)
    modal.show()
  }

  // Process message
  ws.onmessage = ({ data }) => {
    const m = JSON.parse(data)
    if (m.type === ACCEPT_UPDATE_MEM_LIST) {
      setMembers(m.members)
      setName(m.recipient)
      setStatus(m.status)
    }
  }

  // Terminate
  ws.onerror = e => {
    throw new Error('WebSocket error', e)
  }

  return (
    <div id='app'>
      {name
        ? <Lobby
            members={members}
            name={name}
            status={status}
          />
        : <NameForm members={members} ws={ws} />}
      <Footer />
      <DisconnectModal />
    </div>
  )
}

export { ws }
export default App
