import MemberList from './MemberList'

const Lobby = ({ members, name, status }) => (
  <div className='mt-2'>
    <h2>
      <span className='badge bg-secondary rounded-pill'>{members.length}</span> Users in the lobby
    </h2>
    <p>
      Users can be either <code>idle</code>, <code>ready</code>, or <code>busy</code>.
    </p>
    <p>
      Click on a <span className='p-1 list-group-item-danger'>ready user in red</span> to engage.
    </p>
    <p>{status}</p>
    <MemberList
      members={members}
      name={name}
    />
  </div>
)

export default Lobby
