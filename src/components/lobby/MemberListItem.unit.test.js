import { render, screen } from '@testing-library/react'

import MemberListItem from './MemberListItem'

describe('MemberListItem', () => {
  it('should show button when member is self', () => {
    const member = { name: 'abby', status: 'AVAILABLE' }
    const name = 'abby'
    const expectedText = `${member.name} (you) - ${member.status.toLowerCase()}`

    render(<MemberListItem member={member} name={name} />)

    const li = screen.getByRole('listitem')
    const button = screen.getByRole('button')

    expect(li).toContainElement(button)

    expect(button)
      .toHaveTextContent(expectedText)
  })

  it('should show button when member is not self', () => {
    const member = { name: 'abby', status: 'AVAILABLE' }
    const name = 'barbara'
    const expectedText = `${member.name} - ${member.status.toLowerCase()}`

    render(<MemberListItem member={member} name={name} />)

    const li = screen.getByRole('listitem')
    const button = screen.getByRole('button')

    expect(li).toContainElement(button)

    expect(button)
      .toHaveTextContent(expectedText)
  })
})
