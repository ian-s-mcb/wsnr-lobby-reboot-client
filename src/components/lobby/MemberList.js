import MemberListItem from './MemberListItem'

const MemberList = ({ members, name }) => (
  <ul id='memberList'>
    {members.map(m => (
      <MemberListItem key={m.name} member={m} name={name} />
    ))}
  </ul>
)

export default MemberList
