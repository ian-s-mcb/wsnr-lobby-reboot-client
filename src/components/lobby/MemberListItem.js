import {
  STATUS_AVAIL
} from '../../constants'

function MemberListItem ({ member, name }) {
  const objIsReady = (
    (member.status === STATUS_AVAIL) &&
    (member.name !== name)
  )

  const objIsMe = member.name === name
  const displayName = `${member.name} ${objIsMe ? ' (you)' : ''}`

  return (
    <li className='member-list-item'>
      <button
        className={`
          list-group-item
          list-group-item-action
          list-group-item-danger
          ${objIsReady ? '' : 'disabled'}
          ${objIsMe ? 'text-info' : ''}
        `}
        data-bs-toggle='tooltip'
        data-bs-placement='top'
        title='Engage user'
        key={member.name}
      >
        {displayName} - {member.status.toLowerCase()}
      </button>
    </li>
  )
}

export default MemberListItem
