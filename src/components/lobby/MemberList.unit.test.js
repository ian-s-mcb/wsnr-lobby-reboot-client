import { render, screen } from '@testing-library/react'

import MemberListItem from './MemberListItem'
import MemberList from './MemberList'
jest.mock('./MemberListItem')

const MockMemberListItem = ({ member, name }) => (
  <li />
)

beforeEach(() =>
  MemberListItem.mockImplementation(MockMemberListItem)
)

describe('MemberList', () => {
  it('should show empty list if no members', () => {
    const members = []
    const name = ''

    render(<MemberList members={members} name={name} />)

    expect(screen.getByRole('list')).toBeEmptyDOMElement()
  })

  it('should show list of one member', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' }
    ]
    const name = 'abby'

    render(<MemberList members={members} name={name} />)

    const list = screen.getByRole('list')
    const lis = screen.getAllByRole('listitem')
    expect(list).not.toBeEmptyDOMElement()
    lis.forEach(li => expect(list).toContainElement(li))
    expect(MemberListItem)
      .toHaveBeenCalledTimes(1)
    expect(MemberListItem)
      .toHaveBeenNthCalledWith(
        1,
        { member: members[0], name },
        expect.any(Object)
      )
  })

  it('should show list of three members', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const name = 'abby'

    render(<MemberList members={members} name={name} />)

    const list = screen.getByRole('list')
    const lis = screen.getAllByRole('listitem')
    expect(list).not.toBeEmptyDOMElement()
    lis.forEach(li => expect(list).toContainElement(li))
    expect(MemberListItem)
      .toHaveBeenCalledTimes(3)
    expect(MemberListItem)
      .toHaveBeenNthCalledWith(
        1,
        { member: members[0], name },
        expect.any(Object)
      )
    expect(MemberListItem)
      .toHaveBeenNthCalledWith(
        2,
        { member: members[1], name },
        expect.any(Object)
      )
    expect(MemberListItem)
      .toHaveBeenNthCalledWith(
        3,
        { member: members[2], name },
        expect.any(Object)
      )
  })
})
