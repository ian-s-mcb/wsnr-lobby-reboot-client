import { render, screen } from '@testing-library/react'

import MemberList from './MemberList'
import Lobby from './Lobby'
jest.mock('./MemberList')

const MockMemberList = ({ members, name }) => (
  <ul />
)

beforeEach(() =>
  MemberList.mockImplementation(MockMemberList)
)

describe('Lobby', () => {
  it('should show member count of one', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' }
    ]
    const name = 'abby'
    const status = 'AVAILABLE'
    const expectedText = `${members.length} Users in the lobby`

    render(<Lobby members={members} name={name} status={status} />)

    const h2 = screen.getByRole(
      'heading',
      { level: 2, name: expectedText }
    )
    expect(h2).toBeInTheDocument()
  })

  it('should call MemberList with one member', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' }
    ]
    const name = 'abby'
    const status = 'AVAILABLE'

    render(<Lobby members={members} name={name} status={status} />)

    expect(MemberList)
      .toHaveBeenCalledTimes(1)
    expect(MemberList)
      .toHaveBeenNthCalledWith(
        1,
        { members, name },
        expect.any(Object)
      )
  })

  it('should show member count of three', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const name = 'abby'
    const status = 'AVAILABLE'
    const expectedText = `${members.length} Users in the lobby`

    render(<Lobby members={members} name={name} status={status} />)

    const h2 = screen.getByRole(
      'heading',
      { level: 2, name: expectedText }
    )
    expect(h2).toBeInTheDocument()
  })

  it('should call MemberList with three members', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const name = 'abby'
    const status = 'AVAILABLE'

    render(<Lobby members={members} name={name} status={status} />)

    expect(MemberList)
      .toHaveBeenCalledTimes(1)
    expect(MemberList)
      .toHaveBeenNthCalledWith(
        1,
        { members, name },
        expect.any(Object)
      )
  })
})
