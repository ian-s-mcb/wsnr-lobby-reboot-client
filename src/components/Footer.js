import {
  PROFILE_URL,
  SOURCE_URL_CLIENT,
  SOURCE_URL_LICENSE,
  SOURCE_URL_SERVER
} from '../constants'

const Footer = () => (
  <footer className='text-secondary'>
    <p>
      Built by <a href={PROFILE_URL}>ian-s-mcb</a>.
    </p>
    <p>
      The <a href={SOURCE_URL_SERVER}>server</a> and <a
      href={SOURCE_URL_CLIENT}>client</a> components of this web
      application are © 2021 under the terms of the <a
      href={SOURCE_URL_LICENSE}>MIT License</a>.
    </p>
  </footer>
)

export default Footer
