import { Formik, Form, useField } from 'formik'
import * as Yup from 'yup'

import { EMIT_RENAME } from '../constants'

const TextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props)

  // Determine validation
  const validText = 'Looks good!'
  let inputClass = ''
  let feedbackClass = ''
  let showFeedback = false
  if (meta.error) {
    inputClass = 'is-invalid'
    feedbackClass = 'invalid-feedback'
    showFeedback = true
  } else if (field.value.length) {
    inputClass = 'is-valid'
    feedbackClass = 'valid-feedback'
    showFeedback = true
  }

  return (
    <>
      <label className='form-label' htmlFor={props.name}>
        {label}
      </label>
      <input
        id={props.name}
        className={`text-input form-control ${inputClass}`}
        {...field}
        {...props}
      />
      {showFeedback &&
        <div className={feedbackClass}>
          {meta.error || validText}
        </div>}
    </>
  )
}

const NameForm = ({ members, ws }) => (
  <Formik
    initialValues={{
      name: ''
    }}
    validationSchema={Yup.object({
      name: Yup.string()
        .matches(/^[\w]+$/, 'Only letters and numbers are allowed.')
        .min(3, 'Must be 3 characters or more.')
        .max(15, 'Must be 15 characters or less.')
        .required('Required')
        .test(
          'isAvailable',
          'That name is taken.',
          value => (
            !members
              .map(m => m.name)
              .includes(value)
          )
        )
    })}
    onSubmit={async (values, { setSubmitting }) => {
      ws.send(JSON.stringify({
        type: EMIT_RENAME,
        newName: values.name
      }))
      setSubmitting(false)
    }}
  >
    <Form className='' id='nameForm'>
      <h1 className='fs-2 text-center'>wsnr-lobby</h1>
      <TextInput
        label='Name'
        name='name'
        type='text'
      />
      <button className='btn btn-primary mt-3' type='submit'>
        Submit
      </button>
    </Form>
  </Formik>
)

export default NameForm
