import {
  fireEvent, render, screen, waitFor
} from '@testing-library/react'

import NameForm from './NameForm'
const ws = {
  send: jest.fn()
}

describe('NameForm', () => {
  it('should show heading', () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' }
    ]
    const expectedText = /wsnr-lobby/i

    render(<NameForm members={members} ws={ws} />)

    const h1 = screen.getByRole(
      'heading',
      { level: 1, name: expectedText }
    )
    expect(h1).toBeInTheDocument()
  })

  it('should pass validation with valid, available name', async () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const value = 'daria'
    const expectedMessage = JSON.stringify({
      type: 'RENAME',
      newName: value
    })

    render(<NameForm members={members} ws={ws} />)

    const input = screen.getByLabelText(/name/i)
    const button = screen.getByRole('button', { name: /submit/i })
    fireEvent.change(input, { target: { value } })
    fireEvent.click(button)

    await waitFor(() => {
      expect(ws.send)
        .toHaveBeenCalledTimes(1)
      expect(ws.send)
        .toHaveBeenCalledWith(expectedMessage)
    })
  })

  it('should fail validation with empty name', async () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const value = ''
    const expectedInvalidText = /required/i

    render(<NameForm members={members} ws={ws} />)

    const input = screen.getByLabelText(/name/i)
    const button = screen.getByRole('button', { name: /submit/i })
    fireEvent.change(input, { target: { value } })
    fireEvent.click(button)

    await waitFor(() => {
      expect(ws.send)
        .not.toHaveBeenCalled()
      expect(screen.getByText(expectedInvalidText))
        .toBeInTheDocument()
    })
  })

  it('should fail validation with a taken name', async () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const value = 'abby'
    const expectedInvalidText = /that name is taken/i

    render(<NameForm members={members} ws={ws} />)

    const input = screen.getByLabelText(/name/i)
    const button = screen.getByRole('button', { name: /submit/i })
    fireEvent.change(input, { target: { value } })
    fireEvent.click(button)

    await waitFor(() => {
      expect(ws.send)
        .not.toHaveBeenCalled()
      expect(screen.getByText(expectedInvalidText))
        .toBeInTheDocument()
    })
  })

  it('should fail validation with too few characters', async () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const value = 'da'
    const expectedInvalidText = /must be 3 characters or more/i

    render(<NameForm members={members} ws={ws} />)

    const input = screen.getByLabelText(/name/i)
    const button = screen.getByRole('button', { name: /submit/i })
    fireEvent.change(input, { target: { value } })
    fireEvent.click(button)

    await waitFor(() => {
      expect(ws.send)
        .not.toHaveBeenCalled()
      expect(screen.getByText(expectedInvalidText))
        .toBeInTheDocument()
    })
  })

  it('should fail validation with an invalid character', async () => {
    const members = [
      { name: 'abby', status: 'AVAILABLE' },
      { name: 'barbara', status: 'AVAILABLE' },
      { name: 'carol', status: 'AVAILABLE' }
    ]
    const value = 'dari a'
    const expectedInvalidText = /only letters and numbers are allowed/i

    render(<NameForm members={members} ws={ws} />)

    const input = screen.getByLabelText(/name/i)
    const button = screen.getByRole('button', { name: /submit/i })
    fireEvent.change(input, { target: { value } })
    fireEvent.click(button)

    await waitFor(() => {
      expect(ws.send)
        .not.toHaveBeenCalled()
      expect(screen.getByText(expectedInvalidText))
        .toBeInTheDocument()
    })
  })
})
